<?php if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) { die(); } ?>
<h2 class="fictitle"><?php echo $fic['name']; bf::ficCompleted($fic);?></h2>

<article class="fictext">
	<header>
		<?php
		bf::ficPublished($fic);
		
		bf::ficMeta($fic, 'pairing'); bf::ficMeta($fic, 'characters'); bf::ficMeta($fic, 'tags');
		if(array_key_exists('desc', $fic)) { echo '<p class="ficdesc">'. $fic['desc'] .'</p>'; }

		if(array_key_exists('note', $cmeta))
		{ echo '<p class="authorsnotes"><strong class="note">Note</strong> '. $cmeta['note'] .'</p>'; }
		
		// per-chapter header meta
		if(array_key_exists('chapters', $cmeta)){
			// author's notes
			if(array_key_exists('note', $cmeta['chapters'][$chapter-1]))
				{ echo '<p class="authorsnotes"><strong class="note">Note</strong> '. $cmeta['chapters'][$chapter-1]['note'] .'</p>'; }
		}
		
		if(array_key_exists('ao3', $cmeta)) { echo '<p class="ficmeta ao3link">Also published at the <a href="'. $cmeta['ao3'] .'">Archive of Our Own</a>.</p>'; }
		
		if(array_key_exists('chapters', $cmeta) && array_key_exists('intro', $cmeta['chapters'][$chapter-1]))
			{ echo '<p style="float: right; margin-top: 50px;"><a href="'. $cmeta['chapters'][$chapter-1]['intro'] .'" class="button">🎵 Intro</a></p><div style="clear:both;"></div>'; }
	?>
	</header>
	<hr id="text">
<?php
if(array_key_exists('chapters', $cmeta)){
	echo '<h3 class="chaptertitle">Chapter '. $chapter;
	if(array_key_exists('name', $cmeta['chapters'][$chapter-1]))
		{ echo ': '. $cmeta['chapters'][$chapter-1]['name']; }
	echo '</h3>';
}
include($fictext); ?>
	<hr>
	<footer>
<?php

	// per-chapter footer meta
	if(array_key_exists('chapters', $cmeta)){
		
		// author's notes
		if(array_key_exists('endnote', $cmeta['chapters'][$chapter-1]))
			{ echo '<p class="authorsnotes"><strong class="note">Note</strong> '. $cmeta['chapters'][$chapter-1]['endnote'] .'</p>'; }
			
		// previous chapter
		if($chapter > 1)
			{ echo '<p style="float: left;"><a href="'. $ficloc . ($chapter-1) .'#text" class="button">Previous chapter</a></p>'; }
			
		// next chapter
		if($chapter < count($cmeta['chapters']))
			{ echo '<p style="float: right;"><a href="'. $ficloc . ($chapter+1) .'#text" class="button">Next chapter</a></p>'; }
		
		echo '<div style="clear:both; margin-bottom: 10px;"></div>';
	}
	
	if(array_key_exists('endnote', $cmeta))
	{ echo '<p class="authorsnotes"><strong class="note">Note</strong> '. $cmeta['endnote'] .'</p>'; }
	
	// series navigation
	if($series && array_key_exists('parts', $series) && $part && $part > 0){
		echo '<p style="float: left;"><a href="/read/'. $series['uri'] .'/'. $series['parts'][$part-1]['uri'] .'#text" class="button">Previously in '. $series['name'] .'</a></p>';
	}
	if($series && array_key_exists('parts', $series) && $part !== false && $part < count($series['parts'])-1){
		echo '<p style="float: right;"><a href="/read/'. $series['uri'] .'/'. $series['parts'][$part+1]['uri'] .'#text" class="button">Next in '. $series['name'] .'</a></p>';
	} else {
		echo '<p style="float: right;"><a href="/fic/'. $fandom .'" class="button">Back to works list</a></p>';
	} 
	echo '<div style="clear:both;"></div>';

?>
	</footer>
</article>