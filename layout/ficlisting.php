	<?php if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) { die(); } ?>
	<hr>
	<article class="fic" id="<?php echo $fic['uri']; ?>">
		<header>
			<h3><a href="/read/<?php echo $fic['uri']; ?>"><?php echo $fic['name']; ?></a> <?php bf::ficCompleted($fic); ?></h3>
			<?php bf::ficPublished($fic); bf::ficMeta($fic, 'pairing'); bf::ficMeta($fic, 'characters'); bf::ficMeta($fic, 'tags'); ?>
		</header>
		<?php if(array_key_exists('desc', $fic)): ?> <p class="ficdesc"><?php echo $fic['desc']; ?></p><?php endif; ?>
<?php // is this a series?

	if(array_key_exists('parts', $fic)){
		echo "<ol>";
		foreach($fic['parts'] as $p){ 
		?>
		<li class="ficlist"><article id="<?php echo $p['uri']; ?>">
			<header>
				<h4><a href="/read/<?php echo $fic['uri'] . '/'. $p['uri']; ?>"><?php echo $p['name']; ?></a> <?php bf::ficCompleted($p); ?></h4>
				<?php bf::ficPublished($p); bf::ficMeta($p, 'pairing'); bf::ficMeta($p, 'characters'); bf::ficMeta($p, 'tags'); ?>
			</header>
			<?php if(array_key_exists('desc', $p)): ?> <p class="ficdesc"><?php echo $p['desc']; ?></p><?php endif; ?>
		</article></li>
		<?php
		}
		echo "</ol>";
	}

?>
	</article>