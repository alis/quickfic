<?php if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) { die(); } ?>
</section>

<footer class="pagefooter">
	<p><a href="#">Back to top.</a></p>
	<p>Powered by <a href="https://codeberg.org/alis/quickfic">quickfic</a>.</p>
</footer>
</body>
</html>