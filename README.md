# quickfic
A basic fanfic (or origfic!) archive script, powered by PHP, JSON, and you.

Very oldskool and barebones, but should get you started. Also hopefully relatively easy to modify; I use a version of this script to run my [author site](https://alisfranklin.com), for example.

## Requirements
1. A webhost with PHP.
2. The ability to use `.htaccess` files.
3. A text editor.
4. Basic [JSON](https://www.w3schools.com/js/js_json_syntax.asp), HTML, and CSS skillz. Maybe some PHP if you're feeling fancy.
5. Some fic to upload.

## Install
1. Download and/or clone this repo.
2. Upload to your host.
3. Edit `style/main.css`, `layout/home.php`, `layout/header.php` and `layout/footer.php` as required (see below).
4. Edit `data/fandoms.json` as required (see belower).
5. Populate your `data/` folder (see belowest).
6. Enjoy!

## Customization
### Layout editing
`style/main.css`
This is the page stylesheet. Just basic CSS, baby.

`layout/home.php`
The text displayed on the site index page. Keep the first line intact (everything between the `<?php [...] ?>`), but otherwise this is just HTML. Or whatever you want, I guess. It's your site, you do you.

`layout/header.php`
The content displayed on the top of every page. By default just has a fairly barebones site title and navigation menu listing all the fandoms in `data/fandoms.json` in alphabetical order (output via `<?php $this->navigation(); ?>`). Change it however you want!

`layout/footer.php`
The content displayed at the bottom of every page.

`layout/404.php`
Displayed when a particular fic, chapter, etc. can't be found. (Usually typos in URLs or bots trying to randomly guess file paths.)

`layout/navigation.php`
The output of `<?php $this->navigation(); ?>`, in case you want to change that for some reason.

`layout/fic.php`
The layout of individual fic reading pages. Bit of a messy one (separation of presentation and logic wut), so may need some basic PHP skills to modify.

`layout/ficindex.php`
Series view page, showing all parts of that particular series. Another messy one.

`layout/ficlisting.php`
Per-fandom fic listing. Messy? Yessy.

`layout/ficpage.php`
This is displayed below the title but above the list of fics on the listing page.

## Adding fics
This is done by editing some JSON and HTML files (all the indexes, etc. populate off this).

The script associates all fics to fandoms first, as defined in a big ol' JSON file at `data/fandoms.json`. There are some example entries in the file already to get you started.

### Defining fandoms
Fandoms are described as an array a unique identifiers, which is used as the URL slug and data folder. They have two required properties:

1. `name` *(string)* A human-readable name for the fandom.
2. `fanfic` *(string)* An array of fics written for the fandom.

Example:

```
{
	"mdzs": {
		"name": "Mo Dao Zu Shi",
		"fanfic": [...]
	},
	"wyrdverse": {
		"name": "The Wyrd",
		"fanfic": [...]
	},
	"ffxiv": {
		"name": "Final Fantasy XIV",
		"fanfic": [...]
	}
}
```

### Defining fics and series
Items in `fanfic` may consist of:

1. `uri` *(string, required)* A unique identifier for the fic, used in the URL and as the name for the data files.
2. `name` *(string, required)* A human-readable name for the fic.
3. `desc` *(string)* A human-readable description for the fic. HTML is okay here.
4. `published` *(string)* The date the fic was first published, in the format `YYYY-MM-DD`.
5. `pairing` *(array)* An array of pairings for the fic.
6. `characters` *(array)* An array of other characters appearing in the fic.
7. `tags` *(array)* Any other freeform tags.
8. `parts` *(array)* If this is a series, this is where its parts go. `parts` is basically an array of fanfic objects. It can have all of the metadata values as described above, with the exception of more `parts`.
9. `completed` *(true|false)* Whether the fic is completed or not. If not present, defaults to `true`.

(Arrays must be arrays even if they contain only one element.)

Example:

```
{
	"uri": "books-of-the-wyrd",
	"name": "Books of the Wyrd",
	"desc": "Some bad ideas I had once about setting Norse myths in modern Australia...",
	"published": "2015-07-21",
	"pairing": [ "Loki/Sigyn", "Sigmund/Lain" ],
	"parts": [
		{
			"uri": "liesmith",
			"name": "Liesmith",
			"desc": "Boring nerd Sigmun Sussman's life is about to get much more complicated...",
			"published": "2014-10-07",
			"characters": [ "Baldr", "Em Ivanovich", "Wayne Murphy", "David Sussman", "Muninn" ],
			"tags": [ "urban fantasy", "pining", "resolved sexual tension", "body horror", "Australian levels of swearing" ]
		},
		{
			"uri": "stormbringer",
			"name": "Stormbringer",
			"desc": "After the events of LIESMITH, Lain goes back to Asgard. It goes about as well as can be expected.",
			"published": "2015-07-21",
			"pairing": [ "past Baldr/Nanna" ]
			"characters": [ "Em Ivanovich", "Wayne Murphy", "Muninn", "Thrudr", "Magni", "Modi", "Hel", "Forseti", "all of Asgard basically" ],
			"tags": [ "urban fantasy", "Lain being a cagey bastard", "torture", "monster sex", "all the umlauts" ]
		}
	]
}
```

### Adding chapters
All fandoms need to have a corresponding folder created in `data/` (e.g. `data/mdzs/`, `data/wyrdverse/` and `data/ffxiv/` for the example above).

Fics need to have at least two files, based on their `uri`: `data/fandom-uri/fic-uri.json` and `data/fandom-uri/fic-uri.1`. Note that for series fics, the filenames are the fic URIs, *not* the series URI (so for e.g. `data/wyrdverse/liesmith.json` and `data/wyrdverse/liesmith.1`)

`fic-uri.json` defines metadata for the fic's chapters. It may contain the properties:

1. `ao3` *(string)* A link to the fic's AO3 page.
2. `note` *(string)* Author's notes for the fic. HTML okay.
3. `endnote` *(string)* Author's notes for the end of the fic. HTML okay.
4. `chapters` *(array)* The fic's chapters, if present.

Objects in the `chapters` array appear in order (i.e. the fic's first chapter is the first object in the array, its second chapter is the second object, and so on). They may have the properties:

1. `name` *(string)* A human-readable name for the chapter.
2. `note` *(string)* Author's notes for the chapter. HTML okay.
3. `endnote` *(string)* Author's notes for the end of the chapter. HTML okay.

The actual text of individual fics is stored in `data/fandom-uri/fic-uri.1`. This is just an HTML file (no fancy parsing is done by the script). The trailing number indicates the chapter number. So, for example, a three-chapter fic with the URI of `myfic` would have `myfic.1`, `myfic.2` and `myfic.3` (as well as `myfic.json`).

### ... is that really the best way you could've done it?
Probably not but hopefully it will make more sense with the examples included.

## What next?
There are definitely things here that are janky, weird, buggy, or otherwise don't fit what you need. So change them! Hack the heck outta this thing until it works how you want it to. Or scrap the whole thing and start fresh. Up to you.

Happy coding!