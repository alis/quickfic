<?php

class bf {

	private static $dataDir = 'data/';

	public static function parseJSON($json){
		// path to your JSON file
		$file = bf::$dataDir . $json; 
		$obj = false;
		
		if(file_exists($file)){
			$data = file_get_contents($file); 
			$obj = json_decode($data, true); 
		}

		return $obj;
	}
	
/** FIC META PRINTING STUFF *******************************************/
	public static function ficPublished($arr){
		if(array_key_exists('published', $arr)): ?>
			<p class="ficmeta"><time datetime="<?php echo date( 'c', strtotime($arr['published'])); ?>"><?php echo date( 'F Y', strtotime($arr['published'])); ?></time></p>
<?php endif; 
	}
	
	public static function ficMeta($arr, $k){
		if(array_key_exists($k, $arr)){
			if($k == 'tags') { sort($arr[$k]); }
			
			echo "<ul class=\"fictags ficmeta\">";
			foreach($arr[$k] as $m){
				echo "<li>$m</li>";
			}
			echo "</ul>";
		}
	}
	
	public static function ficCompleted($fic){
		if(array_key_exists('completed', $fic) && $fic['completed'] == 'false'){
			echo " <small class=\"ficsatus\">Unfinished</small>";
		}
	}


/** JSON FILE LOOKUPS *************************************************/
// ... i made the json file in a dumb way but it's too hard to change now (tech debt lol)

	// get fandom uri by fic uri
	public static function getFandom($fic, $fandoms){
		$fandom = null;
		foreach($fandoms as $k => $f){
			if(array_key_exists('fanfic', $f)){
				foreach($f['fanfic'] as $w){
					if($w['uri'] == $fic){ $fandom = $k; }
				}
			}
		}
		return $fandom;
	}
	
	// get series/fic meta array by fandom and fic uri
	public static function getFicmeta($fandom, $fic, $fandoms){
		$ficmeta = null;
		if(array_key_exists($fandom, $fandoms) && array_key_exists('fanfic', $fandoms[$fandom])){
			foreach($fandoms[$fandom]['fanfic'] as $w){
				if($w['uri'] == $fic){ $ficmeta = $w; }
			}
		}
		return $ficmeta;
	}
	
	// get series/fic name
	public static function getSeriesName($uri, $fandoms){
		$ficname = false;
		foreach($fandoms as $fandom){
			foreach($fandom['fanfic'] as $w){
				if($w['uri'] == $uri){ $ficname = $w['name']; }
			}
		}
		return $ficname;
	}
	
	// get series fic meta array by fandom, series and fic uri
	public static function getSeriesFicmeta($fic, $series){
		$ficmeta = false;
		if(array_key_exists('parts', $series)){ 
			foreach($series['parts'] as $w){ 
				if($w['uri'] == $fic){ $ficmeta = $w; }
			}
		}
		return $ficmeta;
	}
	
	// if we're part of a series, what number part are we?
	public static function getSeriesPart($fic, $series){
		$num = false;
		$cur = 0;
		if(array_key_exists('parts', $series)){ 
			foreach($series['parts'] as $w){
				if($w['uri'] == $fic){ $num = $cur; }
				$cur++;
			}
		}
		return $num;
	}
	
	// get fic name
	public static function getFicName($uri, $fandoms){
		$ficname = false;
		foreach($fandoms as $fandom){
			foreach($fandom['fanfic'] as $s){
				if(array_key_exists('parts', $s)){
					foreach($s['parts'] as $w){
						if($w['uri'] == $uri){ $ficname = $w['name']; }
					}
				}
			}
		}
		return $ficname;
	}

}

require('app/layout.php');
?>