<?php

class ficLayout {
	private $templateDir = 'layout/';
	private $ficDir = 'data/';
	private $loc = '';
	private $fandoms = '';

// instantiate
	public function __construct(){
		$this->location();
		$this->fandoms = bf::parseJSON('fandoms.json');
	}

// get the current page location
	public function location(){
		if($_SERVER['REQUEST_URI'] == '/' || !$_SERVER['REQUEST_URI'])
			{ $loc = 'home'; }
		else
			{ $loc = explode( '/', $_SERVER['REQUEST_URI']); }
		
		if(is_array($loc)){
			$sloc = array();
			foreach($loc as $l){
				$sloc[] = preg_replace("/[^a-z0-9-]+/i", "", $l);
			}
			$loc = $sloc;
		}
		
		$this->loc = $loc;
	}
	
	private function getTitle(){
		$title = null;
		if($this->loc == 'home')
			{ $title = ''; }
		elseif($this->loc[1] == 'fic' && array_key_exists($this->loc[2], $this->fandoms))
			{ $title = $this->fandoms[$this->loc[2]]['name']; }
		elseif($this->loc[1] == 'read' && array_key_exists(3, $this->loc)){
			$title = bf::getFicName($this->loc[3], $this->fandoms);
			if(!$title) {
				$title = bf::getSeriesName($this->loc[2], $this->fandoms);
				if(array_key_exists(3, $this->loc)) { $title .= ' - Chapter '. intval($this->loc[3]); }
			} elseif(array_key_exists(4, $this->loc)) { $title .= ' - Chapter '. intval($this->loc[4]); }
		}
		elseif($this->loc[1] == 'read' && array_key_exists(2, $this->loc)){
			$title = bf::getSeriesName($this->loc[2], $this->fandoms);
			if(array_key_exists(3, $this->loc)) { $title .= ' - Chapter '. intval($this->loc[3]); }
		}
		
		return $title;
	}

/** GENERAL SITE HEADER ***********************************************/
	public function header(){
		
		$title = $this->getTitle();
		
		include($this->templateDir .'header.php');
	}

/** GENERAL SITE FOOTER ***********************************************/
	public function footer(){
		include($this->templateDir .'footer.php');
	}

/** NAVIGATION MENU ***************************************************/
	public function navigation($current = ''){
		ksort($this->fandoms);
		
		include($this->templateDir .'navigation.php');
	}

/** MAIN CONTENT SORTER ***********************************************/
	public function content(){
		
		if($this->loc == 'home')
			{ include($this->templateDir .'home.php'); }
			
		elseif(is_array($this->loc) && $this->loc[1] == 'fic')
			{ $this->fandom(); }
			
		elseif(is_array($this->loc) && $this->loc[1] == 'read')
			{ $this->fic(); }
			
		else
			{ include($this->templateDir .'404.php'); }
	}

/** FANDOM INDEX PAGES ************************************************/
	private function fandom() {
		
		if(is_array($this->fandoms) && array_key_exists($this->loc[2], $this->fandoms)){
			$f = $this->fandoms[$this->loc[2]];
			if(array_key_exists('fanfic', $f) && count($f['fanfic']) > 0){
				echo '<h2>'. $f['name'] .' fanfic</h2>';
				
				include($this->templateDir .'ficpage.php');
				foreach($f['fanfic'] as $fic){
					include($this->templateDir .'ficlisting.php');
				}
			} else
				{ echo '<p>Nothing here...</p>'; }
		}
		
		else { include($this->templateDir .'404.php');  }
	}
	
	private function fic(){
		
		$fandom = bf::getFandom($this->loc[2], $this->fandoms);
		if($fandom){
			$fic = bf::getFicmeta($fandom, $this->loc[2], $this->fandoms);

			// is this the index page of a series?
			if(empty($this->loc[3]) && array_key_exists('parts', $fic))
				{ include($this->templateDir .'ficindex.php'); }
			
			// is this a chapter of a series fic?
			elseif(array_key_exists(3, $this->loc) && array_key_exists('parts', $fic)) {
				$chapter = array_key_exists(4, $this->loc) ? $this->loc[4] : '1';
				$chapter = intval($chapter);
				$series = $fic;
				$fic = bf::getSeriesFicmeta($this->loc[3], $series);
				if($fic){
					$part = bf::getSeriesPart($this->loc[3], $series);
					$cmeta = bf::parseJSON($fandom .'/'. $fic['uri'] .'.json');
					$ficloc = '/read/'. $series['uri'] .'/'. $fic['uri'] .'/';
					
					$fictext = $this->ficDir . $fandom .'/'. $fic['uri'] .'.'. $chapter;
					if(file_exists($fictext))
						{ include($this->templateDir .'fic.php'); }
					else { include($this->templateDir .'404.php');  }
				} else { include($this->templateDir .'404.php');  }
			}
			
			// if this a chapter of a non-series fic?
			elseif(array_key_exists(2, $this->loc) && !array_key_exists('parts', $fic)) {
				$chapter = array_key_exists(3, $this->loc) ? $this->loc[3] : '1';
				$chapter = intval($chapter);
				$fictext = $this->ficDir . $fandom .'/'. $fic['uri'] . '.'. $chapter;
				$cmeta = bf::parseJSON($fandom .'/'. $fic['uri'] .'.json');
				$ficloc = '/read/'. $fic['uri'] .'/';
				$part = false;
				$series = false;
				
				if(file_exists($fictext))
					{ include($this->templateDir .'fic.php'); }
				else { include($this->templateDir .'404.php');  }
			}
		}
		
		else { include($this->templateDir .'404.php');  }
		
	}
}

?>